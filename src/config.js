import * as Phaser from 'phaser';

// @ts-ignore
import CollisionPlugin from 'phaser-matter-collision-plugin/src';

export default {
  type: Phaser.AUTO,

  scale: {
    parent: 'content',
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    resolution: 1,
    width: 900,
    height: 480,
  },
  localStorageName: 'bb',
  // backgroundColor: '#f3cca3',
  pixelArt: true,
  antialias: true,
  physics: {
    default: 'matter',
    matter: {
      gravity: { y: 0 }, // Top down game, so no gravity
    },
  },
  plugins: {
    scene: [
      {
        plugin: CollisionPlugin, // The plugin class
        key: 'matterCollision', // Where to store in Scene.Systems, e.g. scene.sys.matterCollision
        mapping: 'matterCollision', // Where to store in the Scene, e.g. scene.matterCollision
      },
    ],
  },
};
