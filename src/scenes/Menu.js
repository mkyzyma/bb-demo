import * as Phaser from 'phaser';
import scenes from './sceneKeys';

export default class extends Phaser.Scene {
  constructor() {
    super({ key: scenes.menu });
  }

  create() {
    this.initObjects();
    this.initEvents();
  }

  initObjects() {
    const x = this.game.renderer.width / 2;
    const y = this.game.renderer.height * 0.4;

    const fontFamily = 'Bangers';

    this.title = this.add.text(x, y, 'Balls & Boxes', {
      fontFamily,
      fontSize: '64px',
    });
    this.title.setOrigin(0.5, 0.5);

    const pos = this.title.getBottomCenter();
    this.start = this.add.text(pos.x, pos.y + 30, 'Start game', {
      fontFamily,
      fontSize: '32px',
      color: 'yellow',
    });
    this.start.setOrigin(0.5, 0.5);
    this.start.setInteractive();
  }

  initEvents() {
    this.start.on('pointerdown', () => {
      const { scene } = this.scene.start(scenes.splash);
      scene.scale.startFullscreen();
    });
  }
}
