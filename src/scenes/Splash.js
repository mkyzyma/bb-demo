import * as Phaser from 'phaser';
import sceneKeys from './sceneKeys';

export default class extends Phaser.Scene {
  constructor() {
    super({ key: sceneKeys.splash });
  }

  preload() {
    this.load.image('ball', 'assets/images/ball.png');
    this.load.image('friend', 'assets/images/friend.png');
    this.load.image('tiles', 'assets/images/tiles.png');
    this.load.tilemapTiledJSON('map', 'assets/levels/bb-demo-01-01.json');
  }

  create() {
    this.scene.start(sceneKeys.game);
  }
}
