import * as Phaser from 'phaser';

import { FULLTILT } from 'fulltilt-ng';

import Ball from '../sprites/Ball';
import Environment from '../environment';
import scenes from './sceneKeys';
import Friend from '../sprites/Friend';

const GyroNorm = require('gyronorm');

// eslint-disable-next-line no-unused-vars
const fulltilt = new FULLTILT();

export default class GameScene extends Phaser.Scene {
  constructor() {
    super({ key: scenes.game });
  }

  create() {
    this.env = new Environment(this);
    this.input.mouse.disableContextMenu();

    this.initWorld();
    this.initCamera();
    this.initBall();
    this.initFriends();
    this.initEvents();
    this.initGyro();
  }

  initWorld() {
    this.tilemap = this.make.tilemap({ key: 'map' });
    this.tileset = this.tilemap.addTilesetImage('bb-demo-1', 'tiles');
    this.worldLayer = this.tilemap.createStaticLayer(0, this.tileset, 0, 0);
    this.worldLayer.setCollisionByProperty({ collides: true });
    this.matter.world.convertTilemapLayer(this.worldLayer);

    this.width = this.worldLayer.width;
    this.height = this.worldLayer.height;
  }

  initCamera() {
    this.cameras.main.setBounds(0, 0, this.width, this.height);
    this.cameras.main.setZoom(2);
  }

  initBall() {
    this.ball = new Ball(this.env, 200, 50);
    this.cameras.main.startFollow(this.ball.sprite, true, 0.5, 0.5);
  }

  initFriends() {
    this.friends = [];
    this.friends.push(new Friend(this.env, 50, 50, this.ball));
    this.friends.push(new Friend(this.env, 150, 150, this.ball));
    this.friends.push(new Friend(this.env, 350, 250, this.ball));
    this.friends.push(new Friend(this.env, 250, 200, this.ball));
    this.friends.push(new Friend(this.env, 550, 250, this.ball));
  }

  initEvents() {
    this.env.input.on('pointerup', pointer => {
      const worldPoint = this.env.pointer.positionToCamera(this.env.camera);

      if (pointer.leftButtonReleased()) {
        this.ball.kick(worldPoint);
      }

      if (pointer.rightButtonReleased()) {
        this.ball.breakEnd();
      }
    });

    this.env.input.on('pointerdown', pointer => {
      if (pointer.backButtonDown()) {
        this.scene.start(scenes.menu);
      } else if (pointer.rightButtonDown()) {
        this.ball.breakStart();
      }
    });
  }

  initGyro() {
    const args = {
      frequency: 100, // ( How often the object sends the values - milliseconds )
      gravityNormalized: true, // ( If the gravity related values to be normalized )
      orientationBase: GyroNorm.GAME, // ( Can be GyroNorm.GAME or GyroNorm.WORLD. gn.GAME returns orientation values with respect to the head direction of the device. gn.WORLD returns the orientation values with respect to the actual north direction of the world. )
      decimalCount: 2, // ( How many digits after the decimal point will there be in the return values )
      // logger: null, // ( Function to be called to log messages from gyronorm.js )
      screenAdjusted: false, // ( If set to true it will return screen adjusted values. )
    };

    let zeroGamma = 0;
    let initialized = false;

    this.gn = new GyroNorm();
    this.gn
      .init(args)
      .then(() => {
        this.gn.start(data => {
          if (!initialized) {
            zeroGamma = data.do.gamma;
            initialized = true;
          }
          this.ball.roll(data.do.beta, -(data.do.gamma - zeroGamma));
        });
      })
      .catch(error => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }
}
