import * as Phaser from 'phaser';
import * as WebFont from 'webfontloader';
import sceneKeys from './sceneKeys';

export default class extends Phaser.Scene {
  constructor() {
    super({ key: sceneKeys.boot });
  }

  preload() {
    this.fontsReady = false;
    this.fontsLoaded = this.fontsLoaded.bind(this);
    this.add.text(100, 100, 'loading fonts...');

    this.load.image('loaderBg', './assets/images/loader-bg.png');
    this.load.image('loaderBar', './assets/images/loader-bar.png');

    WebFont.load({
      google: {
        families: ['Bangers'],
      },
      active: this.fontsLoaded,
    });
  }

  update() {
    if (this.fontsReady) {
      this.scene.start(sceneKeys.menu);
    }
  }

  fontsLoaded() {
    this.fontsReady = true;
  }
}
