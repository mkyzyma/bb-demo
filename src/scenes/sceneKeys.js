export default {
  boot: 'BootScene',
  menu: 'MenuScene',
  splash: 'SplashScene',
  game: 'GameScene',
};
