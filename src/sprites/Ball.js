import Phaser from 'phaser';

const Vector = Phaser.Math.Vector2;

export default class Ball {
  constructor(env, x, y) {
    this.env = env;

    this.props = {
      restitution: 0.5,
      friction: 0.01,
      frictionAir: 0.01,
    };

    this.skills = {
      power: 100,
      kickForce: 1,
      breakForce: 10,
      rollForce: 1,
    };

    this.sprite = this.env.scene.matter.add.sprite(x, y, 'ball');
    this.sprite.setCircle(this.sprite.width / 2, this.props);
    this.body = this.sprite.body;
  }

  kick(dest) {
    const divider = 20000;

    const force = (this.skills.power * this.skills.kickForce) / divider;
    const forceVector = new Vector(force, force);

    const deltaVector = dest.subtract(this.sprite.getCenter());
    const normalVector = deltaVector.normalize();

    const resultVector = normalVector.multiply(forceVector);

    this.sprite.applyForce(resultVector);
  }

  roll(x, y) {
    const vector = new Vector(x, y);
    const force = this.skills.rollForce / 50000;
    const forceVector = new Vector(force, force);
    const resultVector = forceVector.multiply(vector);

    this.body.force = new Vector(0, 0);

    this.sprite.applyForce(resultVector);
  }

  breakStart() {
    this.body.frictionAir = this.props.frictionAir * this.skills.breakForce;
  }

  breakEnd() {
    this.body.frictionAir = this.props.frictionAir;
  }
}
