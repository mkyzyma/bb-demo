import Phaser from 'phaser';

const { Body, Bodies } = Phaser.Physics.Matter.Matter;

export default class Friend {
  constructor(env, x, y, ball) {
    this.env = env;

    this.sprite = this.env.scene.matter.add.sprite(x, y, 'friend');

    this.mainBody = Bodies.circle(x, y, this.sprite.width / 2);

    this.sensor = Bodies.circle(x, y, this.sprite.width / 2 + 4, {
      isSensor: true,
    });

    this.body = Body.create({
      parts: [this.mainBody, this.sensor],
      frictionStatic: 0,
      frictionAir: 0.02,
      friction: 0.1,
    });

    this.sprite.setExistingBody(this.body);

    this.ball = ball;

    env.scene.matterCollision.addOnCollideStart({
      objectA: ball.sprite,
      objectB: this.mainBody,
      callback: () => {
        this.sprite.destroy();
      },
    });

    env.scene.matterCollision.addOnCollideStart({
      objectA: ball.sprite,
      objectB: this.sensor,
      callback: () => {
        this.mainBody.isSensor = true;
      },
    });

    env.scene.matterCollision.addOnCollideEnd({
      objectA: ball.sprite,
      objectB: this.sensor,
      callback: () => {
        this.mainBody.isSensor = false;
      },
    });
  }
}
