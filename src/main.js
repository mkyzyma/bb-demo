import * as Phaser from 'phaser';

import BootScene from './scenes/Boot';
import MenuScene from './scenes/Menu';
import SplashScene from './scenes/Splash';
import GameScene from './scenes/Game';

import config from './config';

window.game = window.game || {};

const gameConfig = Object.assign(config, {
  scene: [BootScene, MenuScene, SplashScene, GameScene],
});

class Game extends Phaser.Game {
  constructor() {
    super(gameConfig);
  }
}

window.game = new Game();
