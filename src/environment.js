export default class Environment {
  constructor(scene) {
    this.pointer = scene.input.activePointer;
    this.camera = scene.cameras.main;
    this.scene = scene;
    this.input = scene.input;
  }
}
